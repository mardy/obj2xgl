#! /usr/bin/env python
#
# Copyright (c) 2022 Alberto Mardegan <mardy@users.sourceforge.net>
#
# Distributed under the MIT software license, see the accompanying
# file LICENSE or http://www.opensource.org/licenses/mit-license.php.


import logging
import textwrap
from .generator import BaseCppGenerator


log = logging.getLogger('gx')


class GXImGenerator(BaseCppGenerator):
    """ GX (GameCube, Wii) immediate mode """
    name = 'gx_im'

    def generate(self, parser):
        log.debug("GX generator invoked")
        self.data = parser
        with self.open_output('.c') as f:
            self.write_header(f)
            f.write("\n#include <gccore.h>\n")
            functions = self.write_setup_functions(f)
            functions += self.write_drawing_functions(f)

        with self.open_output('.h') as f:
            self.write_header(f)
            self.write_drawing_function_decls(f, functions)

    def write_setup_function(self, f, obj):
        log.debug(f"Writing setup function for object {obj.name}")
        return None  # TODO: self.write_vertices_format(f, obj)

    def write_drawing_function(self, f, obj):
        log.debug(f"Writing function for object {obj.name}")
        self.current_object = obj
        func_name = self.prefix + '_draw_' + self.escape(obj.name)
        f.write(textwrap.dedent(f"""\

        void {func_name}()
        {{
        """))
        for mat_name, mat_faces in obj.face_materials.items():
            faces = [obj.faces[i] for i in mat_faces]
            material = self.data.materials[mat_name] if mat_name else None
            self.write_faces(f, faces, material)
        f.write("}\n")
        return func_name

    def write_faces(self, f, faces, material):
        data = self.data
        vertex_color = None
        if material:
            # TODO: consider using GX_VA_CLR1 if needed
            if material.ambient_on:
                vertex_color = material.ambient
            if material.diffuse_on:
                vertex_color = material.diffuse

        def write_color(color):
            # Convert to u8 as it's the hardware native format
            c = [int(x * 255) for x in color]
            f.write("    GX_Color3u8({}, {}, {});\n".format(*c))

        for face in faces:
            shape = 'GX_QUADS' if len(face) == 4 else 'GX_TRIANGLES'
            vformat = 0  # TODO: use the one defined in setup()
            n_elems = len(face)
            f.write(f"    GX_Begin({shape}, GX_VTXFMT{vformat}, {n_elems});\n")
            # We use reversed() here since GX defines front-facing triangles to
            # be clockwise
            for v_data in reversed(face):
                v = data.vertexes[v_data[0] - 1]
                f.write("    GX_Position3f32({}, {}, {});\n".format(*v))
                if data.normals:
                    n = data.normals[v_data[2] - 1]
                    f.write("    GX_Normal3f32({}, {}, {});\n".format(*n))
                if vertex_color:
                    write_color(vertex_color)
            f.write("    GX_End();\n")
