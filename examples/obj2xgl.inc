SELF_DIR := $(dir $(lastword $(MAKEFILE_LIST)))

OBJ2XGL_MODULE = $(SELF_DIR)/../obj2xgl/
OBJ2XGL = PYTHONPATH=$(SELF_DIR)/.. python3 -m obj2xgl

DATA_DIR=$(SELF_DIR)/data

CXXFLAGS = -std=c++11 -g -fPIC
LFLAGS = -shared -pie

obj2xgl_%.c obj2xgl_%.h: $(DATA_DIR)/%.obj $(OBJ2XGL_MODULE)
	$(OBJ2XGL) -n $(basename $(notdir $@)) -f $(OBJ2XGL_FORMAT) \
	    $(OBJ2XGL_OPTIONS) $<
