#include "obj2xgl_sample.h"

#include <gccore.h>
#include <malloc.h>
#include <math.h>
#include <ogc/lwp_watchdog.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wiiuse/wpad.h>

#define DEFAULT_FIFO_SIZE (256 * 1024)

static void *frameBuffer[2] = { NULL, NULL };
GXRModeObj *rmode;

void setup_light(Mtx view)
{
    guVector light_position = { 6.0, 4.0, -10.0 };
    guVector light_look = { 0.0, 0.0, 0.0 };
    guVector light_dir;
    GXColor light_diffuse = { 255 * 0.8, 0, 0, 255 };

    // The OpenGL example uses 0.6 as light ambient, and materials are 0.2 by
    // default
    const u8 ambient = 255 * 0.6 * 0.2;
    GXColor light_ambient = { ambient, ambient, ambient, 255 };
    GXColor material_color = { 255, 255, 255, 255 };
    GXLightObj lobj;
    guVecMultiply(view, &light_look, &light_look);
    guVecMultiply(view, &light_position, &light_position);
    guVecSub(&light_look, &light_position, &light_dir);

    GX_InitLightPos(&lobj,
                    light_position.x * 1000,
                    light_position.y * 1000,
                    light_position.z * 1000);
    GX_InitLightDir(&lobj,
                    -light_dir.x,
                    -light_dir.y,
                    -light_dir.z);
    GX_InitLightColor(&lobj, light_diffuse);
    GX_LoadLightObj(&lobj, GX_LIGHT0);

    GX_SetChanAmbColor(GX_COLOR0A0, light_ambient);
    GX_SetChanMatColor(GX_COLOR0A0, material_color);
}

//---------------------------------------------------------------------------------
int main(int argc, char **argv)
{
    //---------------------------------------------------------------------------------
    f32 yscale;

    u32 xfbHeight;

    Mtx view;
    Mtx44 perspective;
    Mtx model, modelview;

    u32 fb = 0; // initial framebuffer index
    GXColor background = { 255, 255, 255, 0xff };

    // init the vi.
    VIDEO_Init();
    WPAD_Init();
    WPAD_SetDataFormat(WPAD_CHAN_ALL, WPAD_FMT_BTNS_ACC_IR);

    rmode = VIDEO_GetPreferredMode(NULL);

    // allocate 2 framebuffers for double buffering
    frameBuffer[0] = MEM_K0_TO_K1(SYS_AllocateFramebuffer(rmode));
    frameBuffer[1] = MEM_K0_TO_K1(SYS_AllocateFramebuffer(rmode));

    VIDEO_Configure(rmode);
    VIDEO_SetNextFramebuffer(frameBuffer[fb]);
    VIDEO_SetBlack(FALSE);
    VIDEO_Flush();
    VIDEO_WaitVSync();
    if (rmode->viTVMode & VI_NON_INTERLACE) VIDEO_WaitVSync();

    // setup the fifo and then init the flipper
    void *gp_fifo = NULL;
    gp_fifo = memalign(32, DEFAULT_FIFO_SIZE);
    memset(gp_fifo, 0, DEFAULT_FIFO_SIZE);

    GX_Init(gp_fifo, DEFAULT_FIFO_SIZE);

    // clears the bg to color and clears the z buffer
    GX_SetCopyClear(background, 0x00ffffff);

    // other gx setup
    GX_SetViewport(0, 0, rmode->fbWidth, rmode->efbHeight, 0, 1);
    yscale = GX_GetYScaleFactor(rmode->efbHeight, rmode->xfbHeight);
    xfbHeight = GX_SetDispCopyYScale(yscale);
    GX_SetScissor(0, 0, rmode->fbWidth, rmode->efbHeight);
    GX_SetDispCopySrc(0, 0, rmode->fbWidth, rmode->efbHeight);
    GX_SetDispCopyDst(rmode->fbWidth, xfbHeight);
    GX_SetCopyFilter(rmode->aa, rmode->sample_pattern, GX_TRUE, rmode->vfilter);
    GX_SetFieldMode(
        rmode->field_rendering,
        ((rmode->viHeight == 2 * rmode->xfbHeight) ? GX_ENABLE : GX_DISABLE));

    GX_SetCullMode(GX_CULL_BACK);
    GX_CopyDisp(frameBuffer[fb], GX_TRUE);
    GX_SetDispCopyGamma(GX_GM_1_0);

    // setup the vertex descriptor
    // tells the flipper to expect direct data
    GX_ClearVtxDesc();
    GX_SetVtxDesc(GX_VA_POS, GX_DIRECT);
    GX_SetVtxDesc(GX_VA_NRM, GX_DIRECT);

    // setup the vertex attribute table
    // describes the data
    // args: vat location 0-7, type of data, data format, size, scale
    // so for ex. in the first call we are sending position data with
    // 3 values X,Y,Z of size F32. scale sets the number of fractional
    // bits for non float data.
    GX_SetVtxAttrFmt(GX_VTXFMT0, GX_VA_POS, GX_POS_XYZ, GX_F32, 0);
    GX_SetVtxAttrFmt(GX_VTXFMT0, GX_VA_NRM, GX_NRM_XYZ, GX_F32, 0);

    GX_SetNumChans(1);
    GX_SetTevOp(GX_TEVSTAGE0, GX_PASSCLR);
    GX_SetChanCtrl(GX_COLOR0A0, GX_ENABLE, GX_SRC_REG, GX_SRC_REG, GX_LIGHT0,
                   GX_DF_SIGNED, GX_AF_NONE);

    // setup our camera at the origin
    // looking down the -z axis with y up
    guVector cam = { 0.0F, 1.0F, -5.0F },
             up = { 0.0F, 1.0F, 0.0F },
             look = { 0.0F, 0.0F, 0.0F };

    guLookAt(view, &cam, &up, &look);

    // setup our projection matrix
    // this creates a perspective matrix with a view angle of 90,
    // and aspect ratio based on the display resolution
    f32 w = rmode->viWidth;
    f32 h = rmode->viHeight;
    guPerspective(perspective, 60, (f32)w / h, 0.1F, 10.0F);
    GX_LoadProjectionMtx(perspective, GX_PERSPECTIVE);

    // do this before drawing
    GX_SetViewport(0, 0, rmode->fbWidth, rmode->efbHeight, 0, 1);

    u32 last_timestamp = gettick();
    guMtxIdentity(model);
    guVector x_axis = { 1.0, 0.0, 0.0 };
    guVector y_axis = { 0.0, 1.0, 0.0 };

    while (1) {

        WPAD_ScanPads();

        u32 timestamp = gettick();
        u32 elapsed_ms =
            ticks_to_millisecs(diff_ticks(last_timestamp, timestamp));

        int h_rotation = 0, v_rotation = 0;
        u32 buttons0 = WPAD_ButtonsHeld(0);
        if (buttons0 & WPAD_BUTTON_HOME) exit(0);
        if (buttons0 & WPAD_BUTTON_LEFT) h_rotation = -1;
        if (buttons0 & WPAD_BUTTON_RIGHT) h_rotation = 1;
        if (buttons0 & WPAD_BUTTON_UP) v_rotation = -1;
        if (buttons0 & WPAD_BUTTON_DOWN) v_rotation = 1;

        if (h_rotation != 0) {
            f32 angle = h_rotation * (float)elapsed_ms / 10.0;
            Mtx rotation_mtx;
            guMtxRotAxisDeg(rotation_mtx, &y_axis, angle);
            guMtxConcat(rotation_mtx, model, model);
        }

        if (v_rotation != 0) {
            f32 angle = -v_rotation * (float)elapsed_ms / 10.0;
            Mtx rotation_mtx;
            guMtxRotAxisDeg(rotation_mtx, &x_axis, angle);
            guMtxConcat(rotation_mtx, model, model);
        }

        setup_light(view);

        guMtxConcat(view, model, modelview);
        GX_LoadPosMtxImm(modelview, GX_PNMTX0);

        Mtx mvi;
        guMtxInverse(modelview, mvi);
        guMtxTranspose(mvi, modelview);
        GX_LoadNrmMtxImm(modelview, GX_PNMTX0);

        obj2xgl_sample_draw_all();

        // do this stuff after drawing
        GX_DrawDone();

        fb ^= 1; // flip framebuffer
        GX_SetZMode(GX_TRUE, GX_LEQUAL, GX_TRUE);
        GX_SetColorUpdate(GX_TRUE);
        GX_CopyDisp(frameBuffer[fb], GX_TRUE);

        VIDEO_SetNextFramebuffer(frameBuffer[fb]);

        VIDEO_Flush();

        VIDEO_WaitVSync();

        last_timestamp = timestamp;
    }
    return 0;
}
