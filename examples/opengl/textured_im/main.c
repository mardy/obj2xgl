#include "obj2xgl_textured_cube.h"

#include <GL/glut.h>

static int h_rotation = 0;
static int v_rotation = 0;

void init()
{
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glEnable(GL_DEPTH_TEST);

    GLfloat light0_direction[] = { .0, -1.0, .0, 0.0 };
    GLfloat light_ambient[] = { .5, .5, .5, 1.0 };
    GLfloat light_diffuse[] = { .5, .5, .5, 1.0 };
    glShadeModel(GL_SMOOTH);

    glLightfv(GL_LIGHT0, GL_POSITION, light0_direction);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);

    GLfloat light1_direction[] = { .0, 1.0, .0, 0.0 };
    glLightfv(GL_LIGHT1, GL_POSITION, light1_direction);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient);

    GLfloat light2_direction[] = { -1.0, .0, .0, 0.0 };
    glLightfv(GL_LIGHT2, GL_POSITION, light2_direction);
    glLightfv(GL_LIGHT2, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT2, GL_AMBIENT, light_ambient);

    GLfloat light3_direction[] = { 1.0, .0, .0, 0.0 };
    glLightfv(GL_LIGHT3, GL_POSITION, light3_direction);
    glLightfv(GL_LIGHT3, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT3, GL_AMBIENT, light_ambient);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHT2);
    glEnable(GL_LIGHT3);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, 640.0 / 480, 0.1, 10.0);
    gluLookAt(0.0, 1.0, -5.0, // eye
              0.0, 0.0, 0.0, // center
              0.0, 1.0, 0.0); // up

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glCullFace(GL_FRONT_AND_BACK);
    glDisable(GL_CULL_FACE);
    obj2xgl_textured_cube_setup_all();
}

void display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);

    obj2xgl_textured_cube_draw_all();
    glFlush();
}

void multiply(const float *m, const float *v, float *r)
{
    for (int i = 0; i < 4; i++) {
        r[i] = m[i*4] * v[0] +
               m[i*4+1] * v[1] +
               m[i*4+2] * v[2] +
               m[i*4+3] * v[3];
    }
}

void on_idle()
{
    static int last_timestamp = -1;

    int timestamp = glutGet(GLUT_ELAPSED_TIME);
    if (last_timestamp < 0) {
        last_timestamp = timestamp;
        return;
    }

    int elapsed_ms = timestamp - last_timestamp;
    if (elapsed_ms < 16) return; // 60 FPS

    static GLfloat matrix[16] = {
        1.0, 0.0, 0.0, 0.0,
        0.0, 1.0, 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        0.0, 0.0, 0.0, 1.0,
    };
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadMatrixf(matrix);
    const GLfloat y_axis[] = { 0.0, 1.0, 0.0, 1.0 };
    const GLfloat x_axis[] = { 1.0, 0.0, 0.0, 1.0 };
    if (h_rotation != 0) {
        GLfloat rot[4];
        multiply(matrix, y_axis, rot);
        GLfloat angle_y = h_rotation * elapsed_ms / 10.0;
        glRotatef(angle_y, rot[0], rot[1], rot[2]);
    }
    if (v_rotation != 0) {
        GLfloat rot[4];
        multiply(matrix, x_axis, rot);
        GLfloat angle_x = -v_rotation * elapsed_ms / 10.0;
        glRotatef(angle_x, rot[0], rot[1], rot[2]);
    }

    glGetFloatv(GL_MODELVIEW_MATRIX, matrix);

    display();
    glPopMatrix();
    last_timestamp = timestamp;
}

void on_special_key_pressed(int key, int, int)
{
    switch (key) {
    case GLUT_KEY_LEFT:
        h_rotation = -1; break;
    case GLUT_KEY_RIGHT:
        h_rotation = 1; break;
    case GLUT_KEY_UP:
        v_rotation = -1; break;
    case GLUT_KEY_DOWN:
        v_rotation = 1; break;
    }
}

void on_special_key_released(int key, int, int)
{
    switch (key) {
    case GLUT_KEY_LEFT:
    case GLUT_KEY_RIGHT:
        h_rotation = 0; break;
    case GLUT_KEY_UP:
    case GLUT_KEY_DOWN:
        v_rotation = 0; break;
    }
}

int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitWindowSize(640, 480);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
    glutCreateWindow("OpenGL");
    init();
    glutDisplayFunc(display);
    glutIdleFunc(on_idle);
    glutIgnoreKeyRepeat(1);
    glutSpecialFunc(on_special_key_pressed);
    glutSpecialUpFunc(on_special_key_released);
    glutMainLoop();
    return 0;
}
