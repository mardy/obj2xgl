SELF_DIR := $(dir $(lastword $(MAKEFILE_LIST)))

include $(SELF_DIR)/../obj2xgl.inc

TARGET := $(shell basename $(CURDIR))

GL_LIBS = -lglut $(shell pkg-config --libs gl glu)
GL_CFLAGS = $(shell pkg-config --cflags gl glu)

.PHONY: clean all check

all: $(TARGET)

clean:
	rm -f $(TARGET) obj2xgl_* *.o

check: $(TARGET)
	./$^

$(TARGET): main.c $(GENERATED_SOURCES)
	$(CC) $(CFLAGS) $(GL_CFLAGS) -o $@ $(filter %.c,$^) $(GL_LIBS)
